<?php defined ('BASEPATH') OR exit ('no direct script access allowed');

class karyawan_model extends CI_model
{
	//panggil nama table
	private $_table = "master_karyawan";
	
	public function tampilDataKaryawan()
	{
		//seperti : select * from <nama_table> "cara 1"
		return $this->db->get($this->_table)->result();
	}
	public function tampilDataKaryawan2()
	{
		// CARA 2
		$query = $this->db->query("SELECT * FROM master_karyawan WHERE flag = 1");
		return $query->result();
	}
	public function tampilDataKaryawan3()
	{
		// CARA 3
		$this->db->select('*');
		$this->db->order_by('nik', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	
	public function save()
	{
		$tgl =$this->input->post('tgl');
		$bln =$this->input->post('bln');
		$thn =$this->input->post('thn');
		$tgl_gabung = $thn . "-" . $bln . "-" . $tgl;
		
		$data['nik']	= $this->input->post('nik');
		$data['nama']	= $this->input->post('nama');
		$data['alamat']	= $this->input->post('alamat');
		$data['telp']	= $this->input->post('telp');
		$data['tempat_lahir']	= $this->input->post('tempat_lahir');
		$data['tgl_lahir']	= $tgl_gabung;
		
		$this->db->insert($this->_table, $data);
	}
	
	
	public function detail($nik)
	{
		$this->db->select('*');
		$this->db->where('nik', $nik); 
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();	
	}
	public function update($nik)
	{
		$tgl =$this->input->post('tgl');
		$bln =$this->input->post('bln');
		$thn =$this->input->post('thn');
		$tgl_gabung = $thn . "-" . $bln . "-" . $tgl;
		
		$data['nik']	= $this->input->post('nik');
		$data['nama']	= $this->input->post('nama');
		$data['alamat']	= $this->input->post('alamat');
		$data['telp']	= $this->input->post('telp');
		$data['tempat_lahir']	= $this->input->post('tempat_lahir');
		$data['tgl_lahir']	= $tgl_gabung;
		
		$this->db->where('nik', $nik);
		$this->db->update($this->_table, $data);
	}
	public function delete($nik)
	{
		$this->db->where('nik', $nik);
		$this->db->delete($this->_table);
		
	}
		
}


