<?php defined ('BASEPATH') OR exit ('no direct script access allowed');

class menu_model extends CI_model
{
	//panggil nama table
	private $_table = "master_menu";
	
	
	
	public function tampilDataMenu()
	{
		return $this->db->get($this->_table)->result();
	}
	
	public function cariHargaMenu($kode_menu)
	{
		$query = $this->db->query("SELECT * FROM" . $this->_table . " WHERE flag = 1 AND kode_menu = '$kode_menu'");
		$hasil = $query->result();
		
		foreach($hasil as $data) {
			$harganya = $data->harga;
		}
		return $harganya;
	}
	
	
	
	
	public function save()
	{
		
		$data['kode_menu']	= $this->input->post('kode_menu');
		$data['nama_menu']	= $this->input->post('nama_menu');
		$data['harga']	= $this->input->post('harga');
		$data['keterangan']	= $this->input->post('keterangan');
		$data['flag']	= 1;
		$this->db->insert($this->_table, $data);
	}
	
	
	public function detail($kode_menu)
	{
		$this->db->select('*');
		$this->db->where('kode_menu', $kode_menu); 
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();	
	}
	public function update($kode_menu)
	{
	
		
		$data['kode_menu']	= $this->input->post('kode_menu');
		$data['nama_menu']	= $this->input->post('nama_menu');
		$data['harga']	= $this->input->post('harga');
		$data['keterangan']	= $this->input->post('keterangan');
		$data['flag']	= 1;
		
		$this->db->where('kode_menu', $kode_menu);
		$this->db->update($this->_table, $data);
	}
	public function delete($kode_menu)
	{
		$this->db->where('kode_menu', $kode_menu);
		$this->db->delete($this->_table);
		
	}
	
	
	
	
}


