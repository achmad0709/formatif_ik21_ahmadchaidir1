<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Karyawan extends CI_controller {

	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("karyawan_model");

		
	}
	
	public function index()
	
	{
		$this->listKaryawan();
	}
	
	public function listKaryawan()
	
	{
		$data['data_karyawan'] = $this->karyawan_model->tampilDataKaryawan();
		$this->load->view('homeKaryawan', $data);
	}
	
	
		public function input()
	{
		$data['data_karyawan'] = $this->karyawan_model->tampilDataKaryawan();
		
		if (!empty($_REQUEST)) {
			$m_karyawan = $this->karyawan_model;
			$m_karyawan->save();
			redirect("Karyawan/index", "refresh"); 
		}
		
		
		$this->load->view('InputKaryawan', $data);
	}
	
	   public function detail_karyawan($nik)
	   {
			$data['detail_karyawan']	= $this->karyawan_model->detail($nik);
			$this->load->view('detail_karyawan', $data);   
	   }
	   
	   public function Editkaryawan($nik)
	{
		$data['data_karyawan'] = $this->karyawan_model->tampilDataKaryawan();
		$data['detail_karyawan'] = $this->karyawan_model->detail($nik);
		
		if (!empty($_REQUEST)) {
			$m_karyawan = $this->karyawan_model;
			$m_karyawan->update($nik);
			redirect("Karyawan/index", "refresh"); 
		}
		
		
		$this->load->view('Editkaryawan', $data);
	}
	
	public function deletekaryawan($nik)
	{
		$m_karyawan = $this->karyawan_model;
		$m_karyawan->delete($nik);
		redirect("Karyawan/index", "refresh");
		
	}
	   
}

