<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Menu extends CI_controller {

	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("menu_model");
		
		
	}
	
	public function index()
	
	{
		$this->listMenu();
	}
	
	public function listMenu()
	
	{
		$data['data_menu'] = $this->menu_model->tampilDataMenu();
		$this->load->view('homeMenu', $data);
	}
	
	
		public function input()
	{
		$data['data_menu'] = $this->menu_model->tampilDataMenu();
		
		if (!empty($_REQUEST)) {
			$m_menu = $this->menu_model;
			$m_menu->save();
			redirect("Menu/index", "refresh"); 
		}
		
		
		$this->load->view('InputMenu', $data);
	}
	
	   public function detail_menu($kode_menu)
	   {
			$data['detail_menu']	= $this->menu_model->detail($kode_menu);
			$this->load->view('detail_menu', $data);   
	   }
	   
	   public function EditMenu($kode_menu)
	{
		
		$data['detail_menu'] = $this->menu_model->detail($kode_menu);
		
		if (!empty($_REQUEST)) {
			$m_menu = $this->menu_model;
			$m_menu->update($kode_menu);
			redirect("Menu/index", "refresh"); 
		}
		
		
		$this->load->view('EditMenu', $data);
	}
	
	public function deletemenu($kode_menu)
	{
		$m_menu = $this->menu_model;
		$m_menu->delete($kode_menu);
		redirect("Menu/index", "refresh");
		
	}
	   
}

