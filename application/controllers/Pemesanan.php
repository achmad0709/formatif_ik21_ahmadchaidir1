<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Pemesanan extends CI_controller {

	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("pemesanan_model");
		$this->load->model("karyawan_model");
		$this->load->model("Menu_model");
		
	}
	
	public function index()
	
	{
		$this->listPemesanan();
	}
	
	public function listPemesanan()
	
	{
		$data['data_pemesanan'] = $this->pemesanan_model->tampilDataPemesanan();
		$this->load->view('homePemesanan', $data);
	}
	
	
		public function input()
	{
		$data['data_karyawan'] = $this->karyawan_model->tampilDataKaryawan();
		$data['data_menu'] = $this->Menu_model->tampilDataMenu();
		
		if (!empty($_REQUEST)) {
			$m_pemesanan = $this->pemesanan_model;
			$m_pemesanan->save();
			redirect("Pemesanan/listPemesanan", "refresh"); 
		}
		
		
		$this->load->view('InputPemesanan', $data);
		
		
	}
	
	public function deletepemesanan($id_pemesanan)
	{
		$m_pemesanan = $this->pemesanan_model;
		$m_pemesanan->delete($id_pemesanan);
		redirect("Pemesanan/index", "refresh");
		
	}
	
	
	   
}

